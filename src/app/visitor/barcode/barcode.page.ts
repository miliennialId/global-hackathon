import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-barcode',
  templateUrl: './barcode.page.html',
  styleUrls: ['./barcode.page.scss'],
})
export class BarcodePage implements OnInit {

  elementType : 'url' | 'canvas' | 'img' = 'url';
  value : string = 'Techiediaries';

  constructor() { }

  ngOnInit() {

  }

}
