export class Visitor {

    public name: string;
    public email: string;
    public mobile: string;
    public govtId: string;
    public validFrom: Date;
    public validUpto: Date;

  

}