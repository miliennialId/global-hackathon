import { Component, OnInit } from '@angular/core';
import {Visitor} from '../visitor';
import { HttpClient } from '@angular/common/http';
import { url } from 'inspector';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {


  url:string;
  email:string;
  
  constructor(public visitor: Visitor,public http: HttpClient,private toastController: ToastController) { 
    visitor.validFrom = new Date();
    visitor.validUpto = new Date(new Date().getTime() + (24*60*60*1000));
  }

  ngOnInit() {

  }

  async presentToast(message:string) {
    const toast = await this.toastController.create({
      message: message,
      duration: 2000
    });
    toast.present();
  }

  register(ngForm: any){
    this.url = "https://hackathon-global.firebaseapp.com/visitor/barcode";
    this.email = ngForm.form.value.email;
    let emailServer = "http://10.49.28.202:8080/meramail/test.php";
    let response = this.sendEmail(emailServer,this.url,this.email);
    if(response != null){
      this.presentToast("Mail sent successfully");
    }
  }

  sendEmail(emailServer,url,email){
    return this.http.post<any>(emailServer,{url,email});
  }

}
